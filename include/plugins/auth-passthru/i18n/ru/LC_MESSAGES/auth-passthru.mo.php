<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: ru
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Russian
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Позволяет HTTP-серверу (Apache или IIS) самому  аутентифицировать пользователя. osTicket будет сопоставлять это определенное имя с внутренним списком пользователей.',
  'Authentication Modes' => 'Режимы проверки подлинности',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Режимы проверки подлинности для клиентов и сотрудников  может быть включены независимо друг от друга. Поиск Клиента может работать и через отдельный механизм (например, LDAP)',
  'Client Authentication' => 'Проверка подлинности клиента',
  'Configuration updated successfully' => 'Конфигурация успешно обновлена',
  'Enable authentication and discovery of clients' => 'Включить проверку подлинности и поиск Клиентов',
  'Enable authentication of staff members' => 'Включить проверку подлинности сотрудников',
  'HTTP Passthru Authentication' => 'Сквозная проверка подлинности HTTP',
  'Staff Authentication' => 'Проверка подлинности для Персонала',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:57 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);