<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n > 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: fr
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: French
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally',
  'Authentication Modes' => 'Modes d\'authentification',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)',
  'Client Authentication' => 'Authentification du client',
  'Configuration updated successfully' => 'Configuration mise à jour avec succès',
  'Enable authentication and discovery of clients' => 'Activer l\'authentification et la découverte des clients',
  'Enable authentication of staff members' => 'Activer l\'authentification des membres du personnel',
  'HTTP Passthru Authentication' => 'HTTP Passthru Authentication',
  'Staff Authentication' => 'Authentification du personnel',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:56 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);