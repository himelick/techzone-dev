<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: pl
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Polish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Pozwala serwerowi HTTP (Apache i IIS) na \\uwierzytelnianie użytkownika. osTicket będzie dopasowywał do użytkownika z serwera uwierzytelniania, do użytkownika zdefiniowanego wewnętrznie',
  'Authentication Modes' => 'Tryby uwierzytelniania',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Autoryzacja klientów i załogi
użytkowników może być włączona niezależnie. Rozpoznawanie klienta
może być obsługiwana przez oddzielny system (taki jak LDAP)',
  'Client Authentication' => 'Uwierzytelnienie klienta',
  'Configuration updated successfully' => 'Konfiguracja pomyślnie zaktualizowana',
  'Enable authentication and discovery of clients' => 'Włącz uwierzytelnianie i rozpoznawanie klientów',
  'Enable authentication of staff members' => 'Włącz uwierzytelnianie pracowników',
  'HTTP Passthru Authentication' => 'Uwierzytelnianie przez hasło HTTP',
  'Staff Authentication' => 'Obsługa uwierzytelniania',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:57 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);