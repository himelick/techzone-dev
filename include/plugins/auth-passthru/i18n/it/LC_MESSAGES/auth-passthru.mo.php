<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: it
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Italian
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Permette ai server HTTP (apache o IIS) di eseguire
l\'autenticazione dell\'utente. osTicket collegherà l\'username dall\'autenticazione del 
server ad un username definito internamente',
  'Authentication Modes' => 'Modalità di autenticazione',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Le modalità di autenticazione per i clienti e i membri dello staff possono essere attivati indipendemente. La scoperta del client può essere supportata tramite un backend (come un LDAP)',
  'Client Authentication' => 'Autenticazione del client',
  'Configuration updated successfully' => 'Configurazione aggiornata correttamente',
  'Enable authentication and discovery of clients' => 'Attivare l\'autenticazione e la scoperta dei client',
  'Enable authentication of staff members' => 'Abilitare l\'autenticazione dei membri dello staff',
  'HTTP Passthru Authentication' => 'Autenticazione HTTP Passthru',
  'Staff Authentication' => 'Autenticazione del personale',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:56 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);