<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.net

Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: German
PO-Revision-Date: 2014-08-06 15:47-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Erlaubt dem HTTP-Server (Apache oder IIS) die
Benutzerauthentifizierung durchzuführen. osTicket wird den Benutzernamen aus der
Serverauthentifizierung mit einem intern definierten Benutzernamen abgleichen',
  'Authentication Modes' => 'Authentifizierungsmodi',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Authentifizierungsmodi für Benutzer und
Agenten können unabhängig voneinander aktiviert werden. Die Benutzer-Suche
kann über eine separate Methode (z. B. LDAP) unterstützt werden',
  'Client Authentication' => 'Benutzer-Authentifizierung',
  'Configuration updated successfully' => 'Konfiguration erfolgreich aktualisiert',
  'Enable authentication and discovery of clients' => 'Suche und Authentifizierung von Benutzern aktivieren',
  'Enable authentication of staff members' => 'Agenten-Authentifizierung aktivieren',
  'HTTP Passthru Authentication' => 'HTTP-Passthrough-Authentifizierung',
  'Staff Authentication' => 'Agenten-Authentifizierung',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:56 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);