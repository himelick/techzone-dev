<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: pt-PT
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Portuguese
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Permite que o servidor HTTP (Apache ou IIS) faça
a autenticação do utilizador. O osTicket fará coincidir com  nome de utilizador do servidor de autenticação
 com um nome de utilizador definido internamente',
  'Authentication Modes' => 'Modos de autenticação',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Modos de autenticação para clientes e membros da\\equipa podem ser ativados de forma independente. A descoberta de
clientes pode ser suportada  de um back-end separado (como LDAP)',
  'Client Authentication' => 'Autenticação de cliente',
  'Configuration updated successfully' => 'Configuração atualizada com sucesso',
  'Enable authentication and discovery of clients' => 'Activar a autenticação e descoberta de clientes',
  'Enable authentication of staff members' => 'Habilitar a autenticação de membros da equipa',
  'HTTP Passthru Authentication' => 'Autenticação HTTP Passthru',
  'Staff Authentication' => 'Autenticação da equipa',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:57 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);