<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: es-ES
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Spanish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Permite que el servidor HTTP (Apache o IIS) realice la autenticación del usuario. osTicket coincidirá  el nombre de usuario con la autenticación del servidor a un nombre de usuario definido internamente',
  'Authentication Modes' => 'Modos de autenticación',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'El modo de autenticación para clientes y miembros del personal pueden ser activadas independientemente. El descubrimiento de clientes puede estar soportada a través de una base independiente (como LDAP)',
  'Client Authentication' => 'Autenticación de cliente',
  'Configuration updated successfully' => 'Configuración actualizada exitosamente',
  'Enable authentication and discovery of clients' => 'Habilitar la autenticación y el descubrimiento de clientes',
  'Enable authentication of staff members' => 'Habilitar la autenticación de los miembros del personal',
  'HTTP Passthru Authentication' => 'Autenticación de paso a través de HTTP',
  'Staff Authentication' => 'Autentificación del personal',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:57 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);