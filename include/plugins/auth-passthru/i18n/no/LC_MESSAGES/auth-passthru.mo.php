<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: no_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: no
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Norwegian
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Tillater HTTP-serveren (Apache eller IIS) å utføre
 godkjenning av brukeren. osTicket samsvarer med brukernavnet fra 
server godkjenning til et brukernavn definert internt',
  'Authentication Modes' => 'Godkjenning modus',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Godkjenning modus for klienter og ansatt
medlemmer kan aktiveres uavhengig. Klient oppdager
kan støttes via en egen motor (for eksempel LDAP)',
  'Client Authentication' => 'Klientgodkjenning',
  'Configuration updated successfully' => 'Konfigurasjon oppdatert',
  'Enable authentication and discovery of clients' => 'Aktiver godkjenning og oppdagelse av kunder',
  'Enable authentication of staff members' => 'Aktivere godkjenning av ansatte',
  'HTTP Passthru Authentication' => 'HTTP gjennomgang godkjenning',
  'Staff Authentication' => 'Ansatte-godkjenning',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:57 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);