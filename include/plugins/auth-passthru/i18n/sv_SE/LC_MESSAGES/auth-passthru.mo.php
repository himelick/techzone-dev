<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: sv-SE
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Swedish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Tillåter HTTP-server (Apache eller IIS) utför
autentisering av användaren. osTicket matchar användarnamnet från 
server autentisering till ett användarnamn som definieras internt',
  'Authentication Modes' => 'Autentiseringslägen',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Autentiseringslägen för klienter och personal
medlemmar kan aktiveras självständigt. Klienten utforska
kan stödjas via en separat backend (till exempel LDAP)',
  'Client Authentication' => 'Klientautentisering',
  'Configuration updated successfully' => 'Konfigurationen uppdaterades',
  'Enable authentication and discovery of clients' => 'Aktivera autentisering och utforska av klienter',
  'Enable authentication of staff members' => 'Aktivera autentisering av anställda',
  'HTTP Passthru Authentication' => 'HTTP Passthru autentisering',
  'Staff Authentication' => 'Personal-autentisering',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:57 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);