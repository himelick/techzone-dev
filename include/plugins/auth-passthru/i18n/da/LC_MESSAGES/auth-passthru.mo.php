<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: da_DK
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: da
X-Crowdin-File: /auth-passthru/LC_MESSAGES/auth-passthru.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Danish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally' => 'Allows for the HTTP server (Apache or IIS) to perform
the authentication of the user. osTicket will match the username from the
server authentication to a username defined internally',
  'Authentication Modes' => 'Authentication Modes',
  'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)' => 'Authentication modes for clients and staff
members can be enabled independently. Client discovery
can be supported via a separate backend (such as LDAP)',
  'Client Authentication' => 'Client Authentication',
  'Configuration updated successfully' => 'Configuration updated successfully',
  'Enable authentication and discovery of clients' => 'Enable authentication and discovery of clients',
  'Enable authentication of staff members' => 'Enable authentication of staff members',
  'HTTP Passthru Authentication' => 'HTTP Passthru Authentication',
  'Staff Authentication' => 'Staff Authentication',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 10,
    'Table-Size' => 10,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:51:56 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);