<?php

// LSS customization file
// The function getExtraLDAPData retrieves LDAP information from an alternate
// LDAP source so that we have all the needed info to create accounts.
// 
// This file should be located in the root directory of the auth-ldap plugin
// (the directory with authenticate.php).

function getExtraLDAPData($addrs) {
    $addrs = array_map(function ($s) { return explode(':', $s)[1]; }, $addrs);
    $data_source = ldap_connect('ldap.services.wisc.edu');
    ldap_bind($data_source);
    foreach ($addrs as $addr) {
        $search = ldap_search($data_source, 'dc=wisc,dc=edu', 'mail=' . $addr);
        $e = ldap_get_entries($data_source, $search);
        if (count($e) > 1) {
            return array($e[0]['mail'][0], $e[0]['telephonenumber'][0]);
        }
    }
}
?>
