<?php

/**
 * Simple class which represents an association between properties of LDAP
 * users and the users created in osTicket.
 */
class LdapProperty {

    static $properties = array(
        'OpenLDAP' => array(
            'countryName',
            'commonName',
            'domainComponent',
            'facsimileTelephoneNumber',
            'friendlyCountryName',
            'givenName',
            'homeTelephoneNumber',
            'jpegPhoto',
            'localityName',
            'rfc822Mailbox',
            'mobileTelephoneNumber',
            'organizationName',
            'organisationalUnitName',
            'owner',
            'pagerTelephoneNumber',
            'postalAddress',
            'postalCode',
            'surname',
            'stateOrProvinceName',
            'streetAddress',
            'telephoneNumber',
            'userid',
        ),
    );

    static function allLocalFields() {
        static $props = array();

        if (!$props) {
            $uf = UserForm::objects()->one();
            foreach ($uf->getDynamicFields() as $f) {
                if (!$f->get('name'))
                    // Currently, `name` is required for new users
                    continue;
                $props['user.'.$f->get('id')] = array(
                    'field'     => $f,
                    'label'     => 'User' . ' / ' . $f->getLabel(),
                );
            }
        }
        return $props;
    }
}
