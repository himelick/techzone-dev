<ul class="tabs">
    <li><a id="tab1" class="active" href="#authentication">Authentication</a></li>
    <li><a id="tab2" href="#properties">Properties</a></li>
</ul>
<div id="authentication" class="tab_content">
    <table class="form_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <tbody>
    <?php echo $form->render(); ?>
    </tbody></table>
</div>
<div id="properties" class="tab_content" style="display:none">
    <table id="ldap_properties" class="form_table" width="940" border="0" cellspacing="0" cellpadding="2">
    <thead>
        <tr><td colspan="2"><div class="form-header section-break">
            <h3>Local Property Mapping</h3>
            <em>Configure LDAP attribute mapping for local custom fields.</em>
        </div></td></tr>
        <tr><th width="auto">Local Property</th>
        <th width="70%">LDAP Attribute<i class="help-tip icon-question-sign"
        data-title="LDAP Attribute" data-content="This field should contain
        the exact LDAP attribute for users in your LDAP directory. For
        instance, <code>displayName</code> or <code>email</code>. Refer to
        your LDAP management software for the exact names of the attributes
        in your LDAP server"></i></th></tr>
    </thead>
    <tbody id="ldap_properties">
<?php foreach ($this->getProperties() as $p) { ?>

<?php } ?>
    </tbody>
    <tbody>
        <tr><td colspan="2"><div class="form-header section-break">
            <h3>Search Attributes</h3>
            <em>Configure which LDAP attributes to use for searching.</em>
        </div></td></tr>
        <tr><td>User-Id</td><td><input type="checkbox"
            name="search[uid]"/></td></tr>
        <tr><td>Display Name</td><td><input type="checkbox"
            name="search[displayName]"/></td></tr>
        <tr><td>Common Name</td><td><input type="checkbox"
            name="search[cn]"/></td></tr>
    </tbody>
    </table>
</div>
<script type="text/javascript">
$(function() {
  var counter=0,
  addLdapProperty = function(el, key, value) {
    var field = 'field-' + (key || ('new-' + (++counter))),
      attr = 'attr-' + (key || ('new-' + counter)),
      row = $('<tr></tr>').append(
      $('<td></td>').append(
        select.clone().attr({name: field}).val(key)
      )
    ).append(
      $('<td></td>').append(
        $('<input>').attr({
          name: attr,
          type: 'text', size: 40,
          value: value || ''
        })
      )
    ).appendTo(el);
  },
  select = $('<select><option value="">— Select a Property —</option></select>');
<?php foreach (LdapProperty::allLocalFields() as $k=>$info) { ?>
  select.append($('<option>').val(<?php echo JsonDataEncoder::encode($k);
      ?>).text(<?php echo JsonDataEncoder::encode($info['label']); ?>));
<?php }
    foreach ($this->getProperties() as $key=>$value) { ?>
    addLdapProperty($('#ldap_properties'), <?php
        echo JsonDataEncoder::encode($key); ?>, <?php
        echo JsonDataEncoder::encode($value); ?>);
<?php } ?>
  addLdapProperty($('#ldap_properties'));
  addLdapProperty($('#ldap_properties'));
});
</script>
<?php
# vim: se ft=php tabstop=4 expandtab:
