<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: pt-PT
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Portuguese
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: Esperado um endereço IP',
  '%s: Unable to bind to server %s' => '%s: não é possível vincular ao servidor %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(opcional) Servidores DNS para consultar sobre servidores de AD. Útil se o servidor AD não está na mesma rede que esse servidor WEB ou não tem o seu DNS configurado para apontar para os servidores AD',
  'Active Directory or LDAP' => 'Active Directory ou LDAP',
  'Authentication Modes' => 'Modos de autenticação',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Modos de autenticação para clientes e membros da
equipe podem ser habilitados independentemente',
  'Automatically Detect' => 'Detectar automaticamente',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Vincular o DN (nome distinto) para se conectar ao servidor LDAP a fim de executar pesquisas',
  'Client Authentication' => 'Autenticação de cliente',
  'Connection Information' => 'Informações de conexão',
  'DNS Servers' => 'Servidores DNS',
  'Default Domain' => 'Domínio Predefinido',
  'Default domain used in authentication and searches' => 'Domínio predefinido usado na autenticação e nas buscas',
  'Enable authentication of clients' => 'Habilitar a autenticação de clientes',
  'Enable authentication of staff members' => 'Habilitar a autenticação de membros da equipa',
  'Fully-qualified domain name is expected' => 'Domínio completo(Fully-qualified) é esperado',
  'Generic configuration for LDAP' => 'Configuração genérica para LDAP',
  'LDAP Authentication and Lookup' => 'Pesquisa e Autenticação LDAP',
  'LDAP Schema' => 'Esquema do LDAP',
  'LDAP configuration updated successfully' => 'Configuração LDAP actualizada com sucesso',
  'LDAP extension is not available' => 'Extensão LDAP não está disponível',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'Extensão LDAP não está disponível. Por Favor instale ou habilite a extensão `php-ldap` no seu servidor WEB',
  'LDAP servers' => 'Servidores LDAP',
  'Layout of the user data in the LDAP server' => 'Layout dos dados do utilizador no servidor LDAP',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Não há servidores especificados. Especificar um servidor Active Directory ou uma lista de servidores',
  'Not necessary if Active Directory is configured above' => 'Não é necessário se o Active Directory estiver configurado acima',
  'Password' => 'Palavra-passe',
  'Password associated with the DN\'s account' => 'Palavra-passe associada à conta do DN',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Providencia um "backend" de autenticação configurável que funciona contra a Microsoft Active Directory e servidores OpenLdap',
  'Search Base' => 'Base de pesquisa',
  'Search User' => 'Pesquisar Utilizador',
  'Staff Authentication' => 'Autenticação da equipa',
  'This section should be all that is required for Active Directory domains' => 'Esta sessão deve ser tudo o que é requerido para domínios do Active Directory',
  'Unable to connect any listed LDAP servers' => 'Incapaz de ligar a qualquer um dos servidores LDAP listados',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Não é possível encontrar servidores LDAP para este domínio. Tente fornecer um endereço de um dos servidores DNS ou especifique manualmente os servidores LDAP para o domínio abaixo.',
  'Use "server" or "server:port". Place one server entry per line' => 'Use  "servidor" ou "servidor: porta". Coloque uma entrada de servidor por linha',
  'Use TLS' => 'Usar TLS',
  'Use TLS to communicate with the LDAP server' => 'Usar TLS para comunicar com o servidor LDAP',
  'Used when searching for users' => 'Usado ao procurar por utilizadores',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Útil apenas para pesquisas de informação. Não é necessário para autenticação. Note que estes dados não são necessários, se seu servidor permite buscas anónimas',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:26 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);