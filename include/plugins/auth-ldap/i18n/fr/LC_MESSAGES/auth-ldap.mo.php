<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n > 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: fr
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: French
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s : attend une adresse IP',
  '%s: Unable to bind to server %s' => '%s : impossible d\'interroger le serveur %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(facultatif) Les serveurs DNS peuvent interroger les serveurs de l\'AD.
Utile si le serveur de l\'AD n\'est pas sur le même réseau que
ce serveur web ou s\'il n\'a pas ses DNS configurés pour
pointer vers les serveur de l\'AD',
  'Active Directory or LDAP' => 'Active Directory ou LDAP',
  'Authentication Modes' => 'Modes d\'authentification',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Modes d\'authentification pour les clients et les menbres du staff, peuvent être activés indépendamment',
  'Automatically Detect' => 'Détecter automatiquement',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Lien DN (distinguised name,nom unique) pour accéder au serveur LDAP
et effectuer des recherches',
  'Client Authentication' => 'Authentification du client',
  'Connection Information' => 'Informations de connexion',
  'DNS Servers' => 'Serveurs DNS',
  'Default Domain' => 'Domaine par défaut',
  'Default domain used in authentication and searches' => 'Domaine par défaut utilisé pour l\'authentification et les recherches',
  'Enable authentication of clients' => 'Activer l\'authentification des clients',
  'Enable authentication of staff members' => 'Activer l\'authentification des membres du personnel',
  'Fully-qualified domain name is expected' => 'Un nom de domaine complet est attendu',
  'Generic configuration for LDAP' => 'Configuration générique pour LDAP',
  'LDAP Authentication and Lookup' => 'Recherche et authentification LDAP',
  'LDAP Schema' => 'Schéma LDAP',
  'LDAP configuration updated successfully' => 'Configuration LDAP correctement mis à jour',
  'LDAP extension is not available' => 'L\'extension LDAP n\'est pas disponible',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'L\'extension LDAP n\'est pas disponible. Merci
d\'installer ou d\'activer l\'extension \'php-ldap\' sur votre serveur
web',
  'LDAP servers' => 'Serveurs LDAP',
  'Layout of the user data in the LDAP server' => 'Correspondance des données utilisateur sur le serveur LDAP',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Aucun serveur spécifié. Spécifier un domaine
Active Directory ou une liste de serveurs',
  'Not necessary if Active Directory is configured above' => 'Inutile si Active Directory est précédemment configuré',
  'Password' => 'Mot de passe',
  'Password associated with the DN\'s account' => 'Mot de passe associé avec le compte DN',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Fournit une interface configurable d\'authentification
qui fonctionne sur des serveurs de Microsoft Active Directory et OpenLdap',
  'Search Base' => 'Base de recherche',
  'Search User' => 'Rechercher un utilisateur',
  'Staff Authentication' => 'Authentification du personnel',
  'This section should be all that is required for Active Directory domains' => 'Cette section contient toutes les informations nécessaires à l\'utilisation des domaines Active Directory',
  'Unable to connect any listed LDAP servers' => 'Impossible de se connecter à l\'un des serveurs LDAP répertoriés',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Impossible de trouver des serveurs LDAP pour ce domaine. Essayez de donner
une adresse de l\'un des serveurs DNS ou spécifier manuellement ci-dessous
les serveurs LDAP pour ce domaine.',
  'Use "server" or "server:port". Place one server entry per line' => 'Utiliser l\'adresse « serveur » ou « serveur : port ». Placer une entrée de serveur par ligne',
  'Use TLS' => 'Utiliser TLS',
  'Use TLS to communicate with the LDAP server' => 'Utiliser TLS pour communiquer avec le serveur LDAP',
  'Used when searching for users' => 'Utilisé lors de la recherche des utilisateurs',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Utile uniquement pour les recherches d\'information. Pas
nécessaire pour l\'authentification. Notez que ces données ne sont pas
nécessaires si votre serveur permet des recherches anonymes',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:25 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);