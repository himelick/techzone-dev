<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: it
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Italian
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: previsto un indirizzo IP',
  '%s: Unable to bind to server %s' => '%s: Impossibile associare il server %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(opzionale) Server DNS  per eseguire query sui server AD. Utile se l\'AD server non è sulla stessa rete di questo sito web o non ha un DNS configurato per arrivare agli AD server',
  'Active Directory or LDAP' => 'Active Directory o LDAP',
  'Authentication Modes' => 'Modalità di autenticazione',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Le modalità di autenticazione per i clienti e i membri
dello staff possono essere attivati indipententemente',
  'Automatically Detect' => 'Rilevare automaticamente',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Associare ND (nome distinto) per associarlo al server
LDAP per effettuare ricerche',
  'Client Authentication' => 'Autenticazione del client',
  'Connection Information' => 'Informazioni sulla connessione',
  'DNS Servers' => 'Server DNS',
  'Default Domain' => 'Dominio predefinito',
  'Default domain used in authentication and searches' => 'Dominio predefinito utilizzato per l\'autenticazione  e le ricerche',
  'Enable authentication of clients' => 'Abilitare l\'autenticazione del client',
  'Enable authentication of staff members' => 'Abilitare l\'autenticazione dei membri dello staff',
  'Fully-qualified domain name is expected' => 'Il nome di dominio completo è previsto',
  'Generic configuration for LDAP' => 'Configurazione generica per LDAP',
  'LDAP Authentication and Lookup' => 'Ricerca e autenticazione del LDAP',
  'LDAP Schema' => 'Schema LDAP',
  'LDAP configuration updated successfully' => 'Configurazione del LDAP aggiornata con successo',
  'LDAP extension is not available' => 'L\'estensione LDAP non è disponibile',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'L\'estensione LDAP non è disponibile. Per favore installa o abilita l\'estensione "php-ldap" nel tuo web server',
  'LDAP servers' => 'Server LDAP',
  'Layout of the user data in the LDAP server' => 'La disposizione dei dati utente nel server LDAP',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Nessun server specificato. Specificare una Directory attiva del dominio o una lista di server',
  'Not necessary if Active Directory is configured above' => 'Non è necessario se Active Directory è già stato configurato',
  'Password' => 'Password',
  'Password associated with the DN\'s account' => 'Password associata all\'account di ND',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Fornisce una autenticazione backend configurabile che funziona contro Microsoft Active Directory e i server OpenLdap',
  'Search Base' => 'Base di ricerca',
  'Search User' => 'Cerca utente',
  'Staff Authentication' => 'Autenticazione del personale',
  'This section should be all that is required for Active Directory domains' => 'Questa sezione è tutto quello che serve per i domini di Active Directory',
  'Unable to connect any listed LDAP servers' => 'Impossibile connettersi a qualsiasi server LDAP elencato',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Impossibile trovare il server LDAP per questo dominio. Prova ad assegnarli uno degli indirizzi di un server DNS o specifica manualmente il server LDAP per questo dominio sotto.',
  'Use "server" or "server:port". Place one server entry per line' => 'Usare "server" o "server:port". Inserire un solo server per riga',
  'Use TLS' => 'Usa TLS',
  'Use TLS to communicate with the LDAP server' => 'Utilizzare TLS per comunicare con il server LDAP',
  'Used when searching for users' => 'Utilizzato durante la ricerca di utenti',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Utile solo per le ricerche di informazioni. Non
necessario per l\'autenticazione. NOTARE che questi dati non sono
necessari se il tuo server permette ricerche anonime',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:25 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);