<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: no_NO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: no
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Norwegian
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: forventet en IP-adresse',
  '%s: Unable to bind to server %s' => '%s: kan ikke feste til server %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(valgfritt) DNS-servere til å spørre om annonse servere. 
Brukes hvis annonseserver ikke er på samme nettverk som
denne nettserver eller ikke har sin DNS konfigurert til
å peke til annonseserverne',
  'Active Directory or LDAP' => 'Aktiv Katalog eller LDAP',
  'Authentication Modes' => 'Godkjenning modus',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Godkjenning modus for klienter og ansatt
medlem kan være aktivert uavhengig',
  'Automatically Detect' => 'Oppdag automatisk',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Fest DN (unike navn) å feste til LDAP
server for å utføre søk',
  'Client Authentication' => 'Klientgodkjenning',
  'Connection Information' => 'Tilkoblingsinformasjon',
  'DNS Servers' => 'DNS-servere',
  'Default Domain' => 'Standarddomenet',
  'Default domain used in authentication and searches' => 'Standarddomenet brukes i godkjenning og søk',
  'Enable authentication of clients' => 'Aktivere godkjenning av klienter',
  'Enable authentication of staff members' => 'Aktivere godkjenning av ansatte',
  'Fully-qualified domain name is expected' => 'Fullstendige domenenavn forventes',
  'Generic configuration for LDAP' => 'Generisk konfigurasjon for LDAP',
  'LDAP Authentication and Lookup' => 'LDAP-godkjenning og oppslag',
  'LDAP Schema' => 'LDAP-skjema',
  'LDAP configuration updated successfully' => 'LDAP-konfigurasjonen ble oppdatert',
  'LDAP extension is not available' => 'LDAP-tillegget finnes ikke',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'LDAP-filtypen er ikke tilgjengelig. Vennligst
installer eller Aktiver \'php-ldap\' forlengelsen på din nett
server',
  'LDAP servers' => 'LDAP-tjenere',
  'Layout of the user data in the LDAP server' => 'Oppsett av bruker dataene i LDAP-tjeneren',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Ingen servere angitt. Angi en aktiv katalog
domene eller en liste over servere',
  'Not necessary if Active Directory is configured above' => 'Ikke nødvendig hvis Aktiv Katalog er konfigurert over',
  'Password' => 'Passord',
  'Password associated with the DN\'s account' => 'Passord tilknyttet DN-kontoen',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Gir en konfigurerbar godkjenning i administrator
som fungerer mot Microsoft Active Directory og OpenLdap
servere',
  'Search Base' => 'Søk i Base',
  'Search User' => 'Søk bruker',
  'Staff Authentication' => 'Ansatte-godkjenning',
  'This section should be all that is required for Active Directory domains' => 'Denne delen bør være alt som kreves for Aktiv Katalog-domener',
  'Unable to connect any listed LDAP servers' => 'Kan ikke koble til noen listet LDAP-servere',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Finner ikke LDAP-tjenere for dette domenet. Prøv å gi
en adresse til DNS-servere eller manuelt spesifiser
 LDAP-tjenere for dette domenet nedenfor.',
  'Use "server" or "server:port". Place one server entry per line' => 'Bruk "server" eller "server: port". Plasser én Serveroppføring per linje',
  'Use TLS' => 'Bruk TLS',
  'Use TLS to communicate with the LDAP server' => 'Bruk TLS til å kommunisere med LDAP-tjeneren',
  'Used when searching for users' => 'Brukes når du søker etter brukere',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Nyttig for informasjon oppslag. Ikke
nødvendig for godkjenning. Merk at disse dataene er ikke
nødvendig hvis serveren tillater anonyme søk',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:26 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);