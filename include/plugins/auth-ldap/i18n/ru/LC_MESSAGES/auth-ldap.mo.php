<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: ru_RU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: ru
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Russian
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: здесь должен быть IP-адрес',
  '%s: Unable to bind to server %s' => '%s: не удалось выполнить привязку к серверу %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(необязательное) DNS сервер для запросов к AD серверам. Используется, если сервер AD  и web сервер системы находятся в разных сетях или DNS хостинга  не указывает на   AD сервер',
  'Active Directory or LDAP' => 'Служба каталогов Active Directory или LDAP',
  'Authentication Modes' => 'Режимы проверки подлинности',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Режимы проверки подлинности для клиентов и персонала могут быть включены независимо друг от друга',
  'Automatically Detect' => 'Автоматическое обнаружение',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Какой DN (отличительное имя) привязать к серверу LDAP, чтобы выполнять поиск (при отключенном анонимном поиске)',
  'Client Authentication' => 'Проверка подлинности клиента',
  'Connection Information' => 'Сведения о подключении',
  'DNS Servers' => 'DNS-серверы',
  'Default Domain' => 'Домен по умолчанию',
  'Default domain used in authentication and searches' => 'Домен по умолчанию, используемый для проверки подлинности и поиска',
  'Enable authentication of clients' => 'Включить проверку подлинности клиентов',
  'Enable authentication of staff members' => 'Включить проверку подлинности сотрудников',
  'Fully-qualified domain name is expected' => 'здесь должно быть полное доменное имя',
  'Generic configuration for LDAP' => 'Общие настройки для LDAP',
  'LDAP Authentication and Lookup' => 'Проверка подлинности и Поиск LDAP',
  'LDAP Schema' => 'Схема LDAP',
  'LDAP configuration updated successfully' => 'Конфигурация LDAP успешно обновлена',
  'LDAP extension is not available' => 'Расширение LDAP недоступно',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'LDAP расширение недоступно. Пожалуйста установите или включите расширение «php-ldap» на вашем web сервере',
  'LDAP servers' => 'Серверы LDAP',
  'Layout of the user data in the LDAP server' => 'Макет данных пользователя на сервере LDAP',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Серверы не указаны. Введите либо домен Активные Directory, либо список серверов',
  'Not necessary if Active Directory is configured above' => 'Не обязательно, если Active Directory настроена выше',
  'Password' => 'Пароль',
  'Password associated with the DN\'s account' => 'Пароль, аккаунта DN для поиска ',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Обеспечивает настраиваемый механизм проверки подлинности, который работает с серверами Microsoft Active Directory и OpenLdap',
  'Search Base' => 'Основание поиска',
  'Search User' => 'Поиск пользователя',
  'Staff Authentication' => 'Проверка подлинности для Персонала',
  'This section should be all that is required for Active Directory domains' => 'В этом разделе должно быть все, что требуется для доменов Active Directory',
  'Unable to connect any listed LDAP servers' => 'Не удается подключиться ко всем перечисленным серверам LDAP',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Не удается найти серверы LDAP для этого домена. Попробуйте задать адрес одного из серверов DNS или вручную укажите сервера LDAP  для этого домена ниже.',
  'Use "server" or "server:port". Place one server entry per line' => 'Использование «сервер» или «сервер: порт». Одна строка - одна запись',
  'Use TLS' => 'Использовать TLS',
  'Use TLS to communicate with the LDAP server' => 'Использование TLS для связи с сервером LDAP',
  'Used when searching for users' => 'Используется при поиске пользователей',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Используется только для поиска информации. Не нужно для проверки подлинности. Обратите внимание, что эти данные являются не нужны, если ваш сервер позволяет анонимный поиск',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:26 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);