<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: sv-SE
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Swedish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: Förväntade en IP-adress',
  '%s: Unable to bind to server %s' => '%s: det gick inte att binda till server %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(tillval) DNS-servrar för att fråga om AD-servrar. 
Användbart om AD-servern inte är på samma nätverk som 
Denna webbserver eller inte har sin DNS konfigurerad att 
peka till AD-servrar',
  'Active Directory or LDAP' => 'Aktiv katalog eller LDAP',
  'Authentication Modes' => 'Autentiseringslägen',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Autentiseringslägen för klienter och staff
members kan aktiveras självständigt',
  'Automatically Detect' => 'Identifiera automatiskt',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Bind DN (distinguised namn) att binda till LDAP
server som för att utföra sökningar',
  'Client Authentication' => 'Klientautentisering',
  'Connection Information' => 'Anslutningsinformation',
  'DNS Servers' => 'DNS Servrar',
  'Default Domain' => 'Standarddomän',
  'Default domain used in authentication and searches' => 'Standarddomän som används i autentisering och sökningar',
  'Enable authentication of clients' => 'Aktivera autentisering av klienter',
  'Enable authentication of staff members' => 'Aktivera autentisering av anställda',
  'Fully-qualified domain name is expected' => 'Fullt kvalificerat domännamn förväntas',
  'Generic configuration for LDAP' => 'Generisk konfiguration för LDAP',
  'LDAP Authentication and Lookup' => 'LDAP-verifiering och sökning',
  'LDAP Schema' => 'LDAP Schema',
  'LDAP configuration updated successfully' => 'LDAP-konfiguration uppdaterats',
  'LDAP extension is not available' => 'Finns ingen LDAP extension',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'Det finns ingen LDAP extension. Vänligen
installera eller aktivera tillägget "php-ldap" på din web
server',
  'LDAP servers' => 'LDAP servrar',
  'Layout of the user data in the LDAP server' => 'Layout av användardata i LDAP-servern',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Inga servrar anges. Antingen ange en aktiv katalog
domän eller en lista över servrar',
  'Not necessary if Active Directory is configured above' => 'Inte nödvändigt om aktiv katalog har konfigurerats ovan',
  'Password' => 'Lösenord',
  'Password associated with the DN\'s account' => 'Lösenord som är associerat med DN: s konto',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Ger en konfigurerbar autentisering backend
som fungerar mot Microsoft Aktiv katalog och ÖppenLdap
servrar',
  'Search Base' => 'Sökbas',
  'Search User' => 'Sök användare',
  'Staff Authentication' => 'Personal-autentisering',
  'This section should be all that is required for Active Directory domains' => 'Detta avsnitt bör vara allt som behövs för aktiva katalog-domäner',
  'Unable to connect any listed LDAP servers' => 'Det gick inte att ansluta till någon listad LDAP-servrar',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Det gick inte att hitta LDAP-servrar för den här domänen. Prova ge
en adress till en DNS-server eller manuellt ange
LDAP-server för den här domänen nedan.',
  'Use "server" or "server:port". Place one server entry per line' => 'Använd "server" eller "server:port". Placera en server post per rad',
  'Use TLS' => 'Använd TLS',
  'Use TLS to communicate with the LDAP server' => 'Använd TLS för att kommunicera med LDAP-servern',
  'Used when searching for users' => 'Används när du söker efter användare',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Användbar endast för informations uppslag. Inte
behövs för autentisering. OBSERVERA att dessa data inte är
nödvändigt om din server tillåter anonyma sökningar',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:26 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);