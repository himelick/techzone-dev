<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: pl
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Polish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: oczekiwano adresu IP',
  '%s: Unable to bind to server %s' => '%s: nie można powiązać z serwerem %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(opcjonalnie) Serwery DNS do zapytań o inne serwery AD . 
Użyteczne jeżeli serwer AD nie jest w tej samej sieci co serwer WWW 
lub jego DNS nie ma skonfigurowanego odniesienia
 to
serwera AD',
  'Active Directory or LDAP' => 'Usługi Active Directory lub LDAP',
  'Authentication Modes' => 'Tryby uwierzytelniania',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Tryby uwierzytelniania dla klientów i członków załogi
, które mogą być włączone niezależnie',
  'Automatically Detect' => 'Automatyczne wykrywanie',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Przypisz DN (nazwa wyróżniająca) aby powiązać serwer LDAP
 w celu umożliwienia wyszukiwania',
  'Client Authentication' => 'Uwierzytelnienie klienta',
  'Connection Information' => 'Informacje o połączeniu',
  'DNS Servers' => 'Serwery DNS',
  'Default Domain' => 'Domena domyślna',
  'Default domain used in authentication and searches' => 'Domenę domyślną używaną w uwierzytelnianiu i wyszukiwaniu',
  'Enable authentication of clients' => 'Włącz uwierzytelnianie klientów',
  'Enable authentication of staff members' => 'Włącz uwierzytelnianie pracowników',
  'Fully-qualified domain name is expected' => 'Pełni kwalifikowana nazwa domeny jest oczekiwana',
  'Generic configuration for LDAP' => 'Konfiguracja dla LDAP',
  'LDAP Authentication and Lookup' => 'Protokół LDAP uwierzytelnienie i wyszukiwanie',
  'LDAP Schema' => 'Schemat protokołu LDAP',
  'LDAP configuration updated successfully' => 'Konfiguracja protokołu LDAP pomyślnie zaktualizowana',
  'LDAP extension is not available' => 'Rozszerzenie protokołu LDAP nie jest dostępne',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'Rozszerzenie protokołu LDAP nie jest dostępne. Proszę 
zainstalować lub włączyć rozszerzenie "php-ldap" na twoim serwerze WWW.',
  'LDAP servers' => 'Serwery LDAP',
  'Layout of the user data in the LDAP server' => 'Układ danych użytkowników z serwera LDAP',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Brak zdefiniowanych serwerów. Należy/można określić Active Directory lub listę serwerów',
  'Not necessary if Active Directory is configured above' => 'Nie konieczne, jeśli usługa Active Directory jest skonfigurowana powyżej',
  'Password' => 'Hasło',
  'Password associated with the DN\'s account' => 'Hasło skojarzone z kontem DN',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Dostarcza konfigurowalną końcówkę do autoryzacji działającej za pośrednictwem serwerów Microsoft Active Directory i OpenLDAP',
  'Search Base' => 'Podstawa wyszukiwania',
  'Search User' => 'Szukaj użytkownika',
  'Staff Authentication' => 'Obsługa uwierzytelniania',
  'This section should be all that is required for Active Directory domains' => 'W tej sekcji powinny być wymagane dla domeny usługi Active Directory',
  'Unable to connect any listed LDAP servers' => 'Nie można połączyć do któregokolwiek z wymieniony serwerów LDAP',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Nie można znaleźć serwera LDAP dla tej domeny. Spróbuj podać
 adres jednego z serwerów DNS lub ręcznie określić
t serwer LDAP dla tej domeny poniżej.',
  'Use "server" or "server:port". Place one server entry per line' => 'Użyj "serwera" lub "serwer: port". Umieść jeden serwer na linię',
  'Use TLS' => 'Wykorzystanie TLS',
  'Use TLS to communicate with the LDAP server' => 'Użyj TLS do komunikacji z serwerem LDAP',
  'Used when searching for users' => 'Używane podczas wyszukiwania użytkowników',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Przydatne tylko do wyszukiwania informacji. Nie wymagane dla uwierzytelniania. Należy zauważyć, że te dane nie są wymagane, tylko jeżeli twój serwer umożliwia anonimowe wyszukiwanie',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:26 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);