<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.net

Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: German
PO-Revision-Date: 2014-08-06 15:47-0400
',
  '%s: Expected an IP address' => '%s: IP-Adresse erwartet',
  '%s: Unable to bind to server %s' => '%s: Verbindung zum Server %s nicht möglich',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(optional) DNS-Server zur Abfrage der AD-Server.
Hilfreich, wenn der AD-Server nicht im gleichen Netzwerk ist wie
dieser Web-Server oder dessen DNS-Konfiguration nicht auf
die AD-Server verweist',
  'Active Directory or LDAP' => 'Active Directory oder LDAP',
  'Authentication Modes' => 'Authentifizierungsmodi',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Authentifizierungsmodi für Benutzer und
Agenten können unabhängig voneinander aktiviert werden',
  'Automatically Detect' => 'Automatisch erkennen',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Eindeutiger Objektname (DN) zur Anmeldung am LDAP
-Server wird bei der Durchführung von Suchvorgängen verwendet',
  'Client Authentication' => 'Benutzer-Authentifizierung',
  'Connection Information' => 'Verbindungsdaten',
  'DNS Servers' => 'DNS-Server',
  'Default Domain' => 'Standard-Domäne',
  'Default domain used in authentication and searches' => 'Standard-Domäne zur Authentifizierung und Suche',
  'Enable authentication of clients' => 'Benutzer-Authentifizierung aktivieren',
  'Enable authentication of staff members' => 'Agenten-Authentifizierung aktivieren',
  'Fully-qualified domain name is expected' => 'Voll qualifizierter Domänenname wird erwartet',
  'Generic configuration for LDAP' => 'Allgemeine LDAP-Konfiguration',
  'LDAP Authentication and Lookup' => 'LDAP-Authentifizierung und Suche',
  'LDAP Schema' => 'LDAP-Schema',
  'LDAP configuration updated successfully' => 'LDAP-Konfiguration erfolgreich aktualisiert',
  'LDAP extension is not available' => 'LDAP-Erweiterung ist nicht verfügbar',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'LDAP-Erweiterung ist nicht verfügbar. Bitte
installieren oder aktivieren Sie die Erweiterung `php-ldap` auf Ihrem Web-
Server',
  'LDAP servers' => 'LDAP-Server',
  'Layout of the user data in the LDAP server' => 'Layout der Benutzerdaten im LDAP-Server',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'Keine Server angegeben. Geben Sie entweder eine Active Directory
Domäne oder eine Liste von Servern an',
  'Not necessary if Active Directory is configured above' => 'Nicht erforderlich, wenn Active Directory konfiguriert ist (oben)',
  'Password' => 'Passwort',
  'Password associated with the DN\'s account' => 'Passwort des DN-Kontos',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Stellt eine konfigurierbare Authentifizierungsmethode bereit,
die das Microsoft Active Directory und OpenLDAP-
Server unterstützt',
  'Search Base' => 'Suchbasis',
  'Search User' => 'Suchbenutzer',
  'Staff Authentication' => 'Agenten-Authentifizierung',
  'This section should be all that is required for Active Directory domains' => 'In diesem Abschnitt sollte alles vorhanden sein, was für Active Directory Domänen erforderlich ist',
  'Unable to connect any listed LDAP servers' => 'Kann keine Verbindung zu den aufgeführten LDAP-Servern herstellen',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'LDAP-Server für diese Domäne können nicht gefunden werden. Versuchen Sie die Eingabe
einer Adresse von einem Ihrer DNS-Server oder geben Sie manuell
den LDAP-Server für diese Domäne unten an.',
  'Use "server" or "server:port". Place one server entry per line' => 'Verwenden Sie "Server" oder "Server:Portnummer". Maximal ein Server pro Zeile',
  'Use TLS' => 'TLS verwenden',
  'Use TLS to communicate with the LDAP server' => 'TLS verwenden, um mit dem LDAP Server zu kommunizieren',
  'Used when searching for users' => 'Wird beim Suchen nach Benutzern verwendet',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Wird nur für Datenabfragen verwendet. Wird
nicht für die Authentifizierung benötigt. BEACHTEN SIE, dass diese Daten nicht
benötigt werden, wenn Ihr Server anonyme Suchen erlaubt',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:25 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);