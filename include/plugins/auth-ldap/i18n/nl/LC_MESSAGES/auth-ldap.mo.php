<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: nl
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Dutch
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: Expected an IP address',
  '%s: Unable to bind to server %s' => '%s: Unable to bind to server %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers',
  'Active Directory or LDAP' => 'Active Directory or LDAP',
  'Authentication Modes' => 'Authentication Modes',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Authentication modes for clients and staff
members can be enabled independently',
  'Automatically Detect' => 'Automatisch detecteren',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches',
  'Client Authentication' => 'Client Authentication',
  'Connection Information' => 'Connection Information',
  'DNS Servers' => 'DNS-servers',
  'Default Domain' => 'Standaarddomein',
  'Default domain used in authentication and searches' => 'Default domain used in authentication and searches',
  'Enable authentication of clients' => 'Enable authentication of clients',
  'Enable authentication of staff members' => 'Enable authentication of staff members',
  'Fully-qualified domain name is expected' => 'Fully-qualified domain name is expected',
  'Generic configuration for LDAP' => 'Generic configuration for LDAP',
  'LDAP Authentication and Lookup' => 'LDAP Authentication and Lookup',
  'LDAP Schema' => 'LDAP-schema',
  'LDAP configuration updated successfully' => 'LDAP configuration updated successfully',
  'LDAP extension is not available' => 'LDAP extension is not available',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server',
  'LDAP servers' => 'LDAP-servers',
  'Layout of the user data in the LDAP server' => 'Layout of the user data in the LDAP server',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'No servers specified. Either specify a Active Directory
domain or a list of servers',
  'Not necessary if Active Directory is configured above' => 'Not necessary if Active Directory is configured above',
  'Password' => 'Wachtwoord',
  'Password associated with the DN\'s account' => 'Password associated with the DN\'s account',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers',
  'Search Base' => 'Search Base',
  'Search User' => 'Zoek gebruiker',
  'Staff Authentication' => 'Staff Authentication',
  'This section should be all that is required for Active Directory domains' => 'This section should be all that is required for Active Directory domains',
  'Unable to connect any listed LDAP servers' => 'Unable to connect any listed LDAP servers',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.',
  'Use "server" or "server:port". Place one server entry per line' => 'Use "server" or "server:port". Place one server entry per line',
  'Use TLS' => 'TLS gebruiken',
  'Use TLS to communicate with the LDAP server' => 'Use TLS to communicate with the LDAP server',
  'Used when searching for users' => 'Used when searching for users',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:25 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);