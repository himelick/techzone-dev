<?php return array (
  '' => 'Project-Id-Version: osticket-plugins
POT-Create-Date: 2014-07-31 12:17 -0500
Report-Msgid-Bugs-To: support@osticket.com
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: crowdin.com
Plural-Forms: nplurals=2; plural=(n != 1);
X-Crowdin-Project: osticket-plugins
X-Crowdin-Language: es-ES
X-Crowdin-File: /auth-ldap/LC_MESSAGES/auth-ldap.po
Last-Translator: greezybacon <jared@osticket.com>
Language-Team: Spanish
PO-Revision-Date: 2015-04-02 11:50-0400
',
  '%s: Expected an IP address' => '%s: Se espera una dirección IP',
  '%s: Unable to bind to server %s' => '%s: No es posible establecer un enlace con el servidor %s',
  '(optional) DNS servers to query about AD servers.
Useful if the AD server is not on the same network as
this web server or does not have its DNS configured to
point to the AD servers' => '(Opcional) servidores DNS para consultar acerca de servidores AD. Muy útil si el servidor AD no está en la misma red que el servidor web o no tiene su DNS configurado en el punto a los servidores de AD',
  'Active Directory or LDAP' => 'Active Directory o LDAP',
  'Authentication Modes' => 'Modos de autenticación',
  'Authentication modes for clients and staff
members can be enabled independently' => 'Modos de autenticación para clientes y el personal \\ miembros pueden activarse de manera independiente',
  'Automatically Detect' => 'Detectar automáticamente',
  'Bind DN (distinguised name) to bind to the LDAP
server as in order to perform searches' => 'Enlace DN (denominación distinguida) para enlazar con el servidor LDAP con el fin de realizar búsquedas',
  'Client Authentication' => 'Autenticación de cliente',
  'Connection Information' => 'Información de conexión',
  'DNS Servers' => 'Servidor DNS',
  'Default Domain' => 'Dominio por defecto',
  'Default domain used in authentication and searches' => 'Dominio predeterminado que se utiliza en la autenticación y las búsquedas',
  'Enable authentication of clients' => 'Habilitar la autenticación de clientes',
  'Enable authentication of staff members' => 'Habilitar la autenticación de los miembros del personal',
  'Fully-qualified domain name is expected' => 'Se espera que el nombre de dominio sea totalmente cualificado',
  'Generic configuration for LDAP' => 'Configuración generica para LDAP',
  'LDAP Authentication and Lookup' => 'Búsqueda y autenticación LDAP',
  'LDAP Schema' => 'Esquema LDAP',
  'LDAP configuration updated successfully' => 'Configuración LDAP actualizado correctamente',
  'LDAP extension is not available' => 'Extensión LDAP no está disponible',
  'LDAP extension is not available. Please
install or enable the `php-ldap` extension on your web
server' => 'Extensión LDAP no está disponible. Por favor instale o habilite la extension "php-ldap\' en el servidor web',
  'LDAP servers' => 'Servidores LDAP',
  'Layout of the user data in the LDAP server' => 'Diseño de los datos del usuario en el servidor LDAP',
  'No servers specified. Either specify a Active Directory
domain or a list of servers' => 'No hay servidores especificados. Especifique un dominio de Active Directory o una lista de servidores',
  'Not necessary if Active Directory is configured above' => 'No es necesario si se ha configurado por encima de Active Directory',
  'Password' => 'Contraseña',
  'Password associated with the DN\'s account' => 'Contraseña asociada con la cuenta DN',
  'Provides a configurable authentication backend
which works against Microsoft Active Directory and OpenLdap
servers' => 'Proporciona un motor de autenticación configurable que trabaja en contra de Microsoft Active Directory y servidores OpenLDAP',
  'Search Base' => 'Base de búsqueda',
  'Search User' => 'Buscar usuario',
  'Staff Authentication' => 'Autentificación del personal',
  'This section should be all that is required for Active Directory domains' => 'Esta sección debe ser todo lo que se requiere para dominios de Active Directory',
  'Unable to connect any listed LDAP servers' => 'No se puede conectar cualquier lista servidores LDAP',
  'Unable to find LDAP servers for this domain. Try giving
an address of one of the DNS servers or manually specify
the LDAP servers for this domain below.' => 'No es posible encontrar servidores LDAP para este dominio. Trate de dar una dirección de uno de los servidores DNS o especifique manualmente los servidores LDAP para este dominio.',
  'Use "server" or "server:port". Place one server entry per line' => 'Utilice "servidor" o "servidor:puerto". Coloque una entrada de servidor por línea',
  'Use TLS' => 'Usar TLS',
  'Use TLS to communicate with the LDAP server' => 'Utilizar TLS para comunicarse con el servidor LDAP',
  'Used when searching for users' => 'Utilizado en la búsqueda de usuarios',
  'Useful only for information lookups. Not
necessary for authentication. NOTE that this data is not
necessary if your server allows anonymous searches' => 'Información útil sólo para las búsquedas. No es necesario para la autenticación. Tenga en cuenta que este dato no es necesario si el servidor permite búsquedas anónimas',
  0 => 
  array (
    'Revision' => 0,
    'Total-Strings' => 41,
    'Table-Size' => 41,
    'Build-Timestamp' => 'Fri, 10 Apr 15 14:53:26 +0000',
    'Format-Version' => 'A',
    'Encoding' => 'UTF-8',
  ),
);